var selectedRow = null;

function onFormSubmit() {
  if (validate()) {
    var formData = readFormData();
    if (selectedRow == null) insertNewRecord(formData);
    else updateRecord(formData);
    resetForm();
  }
}

function readFormData() {
  var formData = {};
  formData["nomeja"] = document.getElementById("nomeja").value;
  formData["pesanan"] = document.getElementById("pesanan").value;
  formData["jmlh"] = document.getElementById("jmlh").value;
  return formData;
}

function insertNewRecord(data) {
  var table = document
    .getElementById("List")
    .getElementsByTagName("tbody")[0];
  var newRow = table.insertRow(table.length);
  cell1 = newRow.insertCell(0);
  cell1.innerHTML = data.nomeja;
  cell2 = newRow.insertCell(1);
  cell2.innerHTML = data.pesanan;
  cell3 = newRow.insertCell(2);
  cell3.innerHTML = data.jmlh;
  cell3 = newRow.insertCell(3);
  cell3.innerHTML = `<a id="edit" onClick="onEdit(this)">Edit</a>&nbsp;&nbsp;&nbsp<a id="delete" onClick="onDelete(this)">Delete</a>`;
}

function resetForm() {
  document.getElementById("nomeja").value = "";
  document.getElementById("pesanan").value = "";
  document.getElementById("jmlh").value = "";
  selectedRow = null;
}

function onEdit(td) {
  selectedRow = td.parentElement.parentElement;
  document.getElementById("nomeja").value = selectedRow.cells[0].innerHTML;
  document.getElementById("pesanan").value = selectedRow.cells[1].innerHTML;
  document.getElementById("jmlh").value = selectedRow.cells[2].innerHTML;
}
function updateRecord(formData) {
  selectedRow.cells[0].innerHTML = formData.nomeja;
  selectedRow.cells[1].innerHTML = formData.pesanan;
  selectedRow.cells[2].innerHTML = formData.jmlh;
}

function onDelete(td) {
  if (confirm('Are you sure to delete this record ?')) {
      row = td.parentElement.parentElement;
      document.getElementById("List").deleteRow(row.rowIndex);
      resetForm();
  }
}
function validate() {
  isValid = true;
  if (document.getElementById("nomeja").value == "") {
    isValid = false;
    document.getElementById("fullNameValidationError").classList.remove("hide");
  } else {
    isValid = true;
    if (
      !document
        .getElementById("fullNameValidationError")
        .classList.contains("hide")
    )
      document.getElementById("fullNameValidationError").classList.add("hide");
  }
  return isValid;
}

function hanyaAngka(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
  return true;
}
